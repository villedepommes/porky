#!/bin/bash

read -r -d '' SCRIPT_BODY <<- EOS
import sys
import argparse
import ast
import importlib
import re
import os
from collections import defaultdict as dd

def pp(*args, **kwargs):
    print(*args, **kwargs)

def import_modules(module_list):
    for mod in module_list:
        globals()[mod] = importlib.import_module(mod)

def repl_fun(mo):
    return os.environ[mo.group(1)] 

class ModuleCollect(ast.NodeVisitor):
    def __init__(self):
        self._module_list = []

    def visit_Attribute(self, at):
        if isinstance(at.value, ast.Name):
            self._module_list.append(at.value.id)
        else:
            self.generic_visit(at)

bash_var_pattern = r"(?<!\\\$)\\\${([a-zA-Z0-9_]+)}"

parser = argparse.ArgumentParser(description='PyTorch ImageNet Training')
parser.add_argument('-p', '--prologue', metavar='CODE', default='1',
help='prologue code to execute before loop')
parser.add_argument('-e', '--epilogue', metavar='CODE', default='1',
help='epilogue code to execute after loop')
parser.add_argument('body', type=str, metavar='CODE',
help='code inside loop\'s body')
parser.add_argument('-s', '--silent', action="store_true", help='wrap in a print statement')

args = parser.parse_args()

prologue, body, epilogue = map(lambda x: re.sub(bash_var_pattern, repl_fun, x), [args.prologue, args.body, args.epilogue])

exec(prologue)

body_ast = ast.parse(body)
is_assignment = isinstance(body_ast.body, ast.Assign)
mc = ModuleCollect()
mc.visit(body_ast)
import_modules(mc._module_list)

for line in sys.stdin:
    t = line.split()
    l = line
    s = line
    c = len(t)
    try:
        il = int(l)
    except:
        pass
    try:
        fl = float(l)
    except:
        pass
    if not args.silent:
        body = '__result__ = ' + body
    res = exec(body, globals(), locals())
    if not args.silent:
        print(__result__) # noqa

exec(epilogue)
EOS

python3 -c "$SCRIPT_BODY" "$@"